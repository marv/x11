# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: Qt Data Visualization"
DESCRIPTION="Qt Data Visualization module provides a way to visualize data in
3D as bar, scatter, and surface graphs. It is especially useful for
visualizing depth maps and large quantities of rapidly changing data, such as
data received from multiple sensors. The look and feel of graphs can be
customized by using themes or by adding custom items and labels to them.
Qt Data Visualization is built on Qt 5 and OpenGL to take advantage of
hardware acceleration and Qt Quick 2."

LICENCES="GPL-3"

MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? ( x11-libs/qttools:${SLOT}[>=5.11.0] )
    build+run:
        x11-dri/mesa
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtmultimedia:${SLOT}[>=${PV}] )
"

qtdatavis3d_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtdatavis3d_src_compile() {
    default

    option doc && emake docs
}

qtdatavis3d_src_install() {
    default

    if option doc ; then
        dodoc doc/${PN}.qch
        docinto html
        dodoc -r doc/qtdatavisualization
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

