# Copyright 2007 Alexander Færøy <ahf@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Fontconfig is a library for font customization and configuration"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/release/${PNV}.tar.gz"

REMOTE_IDS="freecode:${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv8 ~x86"
MYOPTIONS="
    doc
    ( linguas: zh_CN )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config[>=0.9.0]
        doc? (
            app-text/docbook-sgml-dtd:3.1
            app-text/docbook-utils[jadetex]
        )
    build+run:
        app-arch/bzip2
        dev-libs/expat
        media-libs/freetype:2[>=2.8.1]
        sys-apps/util-linux [[ note = [ for libuuid ] ]]
        sys-libs/zlib
    test:
        dev-libs/json-c
    recommendation:
        fonts/dejavu [[ description = [ Default fonts ] ]]
    suggestion:
        app-admin/eclectic-fontconfig[>=2.0.11] [[
            description = [ Manage fontconfig /etc/fonts/conf.d/ symlinks ]
        ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --localstatedir=/var
    --enable-nls
    # Use expat instead of libxml2
    --disable-libxml2
    --disable-static
    --with-default-hinting=slight
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "doc docs" )

src_install() {
    default

    doman fc-*/fc-*.1 doc/fonts-conf.5
    dodoc doc/fontconfig-user.{txt,pdf}

    # fc-lang/ directory contains language coverage datafiles
    # which are needed to test the coverage of fonts.
    insinto /usr/share/fc-lang
    doins fc-lang/*.orth

    keepdir /var/cache/fontconfig
}

pkg_postinst() {
    if [[ ${ROOT} == "/" && -n $(echo "${ROOT}"/usr/share/fonts/**/**.ttf) ]]; then
        ebegin "Creating font cache"
            fc-cache --really-force --system-only
        eend $?
    fi
}

